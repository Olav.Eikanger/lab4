package no.uib.inf101.bonus;

import java.awt.Color;

import javax.swing.JPanel;

public abstract class ColorSelector extends JPanel {
    public abstract Color[] getColors();
}
