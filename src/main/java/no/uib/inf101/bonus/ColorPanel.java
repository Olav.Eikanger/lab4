package no.uib.inf101.bonus;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.BasicStroke;
import java.awt.Dimension;

import javax.swing.JPanel;

public class ColorPanel extends JPanel {
    private Color color;
    private int sideLength;

    public ColorPanel(Color color, int sideLength) {
        this.color = color;
        this.sideLength = sideLength;
        this.setPreferredSize(new Dimension(sideLength, sideLength));
    }

    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        Graphics2D g2 = (Graphics2D) g;
        g2.setColor(color);
        g2.fillRect(0, 0, sideLength, sideLength);
        g2.setColor(Color.BLACK);
        g2.setStroke(new BasicStroke(sideLength / 10));
        g2.drawRect(0, 0, sideLength, sideLength);
    }

    public void setColor(Color color) {
        this.color = color;
    }

    public Color getColor() {
        return this.color;
    }
}
