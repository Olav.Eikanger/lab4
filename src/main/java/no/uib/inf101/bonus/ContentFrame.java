package no.uib.inf101.bonus;

import java.awt.Color;
import java.awt.event.ActionEvent;

import javax.swing.JButton;
import javax.swing.JFrame;

public class ContentFrame extends JFrame {
    private static final Color[] BASE_COLORS = new Color[]{Color.BLUE, Color.CYAN, Color.GRAY};
    private static final int BASE_DEPTH = 7;
    private RecursivePicture picture;
    private DepthField depthInput;
    private JButton redrawButton;
    private ColorSelector colorPalette;

    public static ContentFrame create() {
        ColorSelector colorPalette = new ColorDisplay(BASE_COLORS, 20);
        RecursivePicture picture = new BeautifulPicture(BASE_COLORS);
        RedrawButton redrawButton = new RedrawButton();
        DepthField depthInput = new DepthField(BASE_DEPTH, 3);
        return new ContentFrame(colorPalette, picture, redrawButton, depthInput);
    }

    private ContentFrame(
        ColorSelector colorDisplay,
        RecursivePicture picture,
        RedrawButton redrawButton,
        DepthField depthInput
    ) {
        this.colorPalette = colorDisplay;
        this.picture = picture;
        this.redrawButton = redrawButton;
        this.depthInput = depthInput;
        this.setTitle("Sierpinski's triangle");
        this.configureComponents();
        this.setResizable(false);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setVisible(true);
    }

    private void redrawPicture(ActionEvent action) {
        int depth;
        try {
            depth = Integer.parseInt(depthInput.getText());
        } catch (NumberFormatException e) {
            return;
        }

        Color[] colors = colorPalette.getColors();
        if (depth < 0) {
            return;
        }
    
        picture.setDepth(depth);
        picture.setColors(colors);
        picture.repaint();
    }

    private void configureComponents() {
        redrawButton.addActionListener(this::redrawPicture);
        this.setContentPane(picture);
        this.add(depthInput);
        this.add(redrawButton);
        this.add(colorPalette);
        this.pack();
    }
}
