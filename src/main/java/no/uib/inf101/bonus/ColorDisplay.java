package no.uib.inf101.bonus;

import java.awt.Color;
import java.awt.event.MouseEvent;

import javax.swing.BoxLayout;
import javax.swing.JColorChooser;
import javax.swing.JDialog;
import javax.swing.event.MouseInputAdapter;

public class ColorDisplay extends ColorSelector {
    private ColorPanel[] colorPanels;
    private JDialog popupSelector;

    public ColorDisplay(Color[] colors, int sideLength) {
        colorPanels = new ColorPanel[colors.length];
        this.setLayout(new BoxLayout(this, BoxLayout.LINE_AXIS));
        for (int i = 0; i < colors.length; i++) {
            ColorPanel colorPanel = new ColorPanel(colors[i], sideLength);
            colorPanel.addMouseListener(new MouseInputAdapter() {
                @Override
                public void mouseClicked(MouseEvent e) {
                    setNewColor(colorPanel);
                }
            });

            colorPanels[i] = colorPanel;
            this.add(colorPanel);
        }
    }

    private void setNewColor(ColorPanel panel) {
        JColorChooser colorChooser = new JColorChooser(panel.getColor());
        this.popupSelector = JColorChooser.createDialog(
            this.getTopLevelAncestor(),
            "Choose a triangle color",
            false,
            colorChooser,
            e -> {
                panel.setColor(colorChooser.getColor());
                panel.repaint();
                popupSelector.dispose();
            },
            e -> {
                /* Does nothing */
            }
        );
        popupSelector.setVisible(true);
    }

    public Color[] getColors() {
        Color[] colors = new Color[this.colorPanels.length];
        for (int i = 0; i < colors.length; i++) {
            colors[i] = colorPanels[i].getColor();
        }

        return colors;
    }
}
