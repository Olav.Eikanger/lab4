package no.uib.inf101.bonus;

import java.awt.Color;

import javax.swing.JPanel;

public abstract class RecursivePicture extends JPanel {
    public abstract void setColors(Color[] colors);

    public abstract void setDepth(int depth);
}
