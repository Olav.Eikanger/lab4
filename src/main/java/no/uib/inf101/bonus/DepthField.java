package no.uib.inf101.bonus;

import javax.swing.JTextField;

public class DepthField extends JTextField {
    public DepthField(int depth, int columns) {
        super(Integer.toString(depth), columns);
    }

    public int getDepth() {
        try {
            return Integer.parseInt(this.getText());
        } catch (NumberFormatException e) {
            return 0;
        }
    }
}
