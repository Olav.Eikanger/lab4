package no.uib.inf101.bonus;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.BasicStroke;
import java.awt.geom.Rectangle2D;

public class BeautifulPicture extends RecursivePicture {
  private static final Rectangle2D CANVAS_BOUNDS = new Rectangle2D.Double(50, 50, 500, 400);
  private Color[] colors;
  private int depth = 7;

  public BeautifulPicture(Color[] colors) {
    this.colors = colors;
    this.setPreferredSize(new Dimension(600, 500));
  }

  /**
   * Recurisvly draws the upside-down triangles
   * @param g2 The object to draw on
   * @param box The box containing the location of the current triangle and the next three in the sequence
   * @param depth How many iterations to do
   * 
   */
  private void drawInvertedTriangles(Graphics2D g2, Rectangle2D box, int depth){
    if (depth == 0) {
      return;
    }

    int[] triangleX = new int[3];
    int[] triangleY = new int[3];

    // Generates the coordinates for the three tips
    // of an upside down triangle

    for (int i = 0; i < 3; i++) {
      triangleX[i] = (int) (box.getX() + (i + 1) * box.getWidth() / 4);
      triangleY[i] = (int) (box.getY() + box.getHeight() / (2 - (i % 2)));
    }

    g2.setColor(getColorByDepth(depth));
    g2.setStroke(new BasicStroke(depth));
    g2.fillPolygon(triangleX, triangleY, 3);
    for (int i = 0; i < 3; i++) {
      Rectangle2D boundingBox =
      new Rectangle2D.Double(
        box.getX() + i * box.getWidth() / 4,
        box.getY() + (box.getHeight() / 2) * ((i + 1) % 2),
        box.getWidth() / 2,
        box.getHeight() / 2
      );
      drawInvertedTriangles(
        g2,
        boundingBox,
        depth - 1
      );
    }
  }

  /**
   * Draws the main triangle
   * @param g2 The object to draw on
   * @param box The box that will contain the main triangle
   */
  private void drawTriangle(Graphics2D g2, Rectangle2D box) {
    int[] triangleX = new int[3];
    int[] triangleY = new int[3];
    for (int i = 0; i < 3; i++) {
      triangleX[i] = (int) (box.getX() + i * box.getWidth() / 2);
      triangleY[i] = (int) (box.getY() + box.getHeight() * ((i + 1) % 2));
    }

    g2.setColor(Color.BLACK);
    g2.setStroke(new BasicStroke(2));
    g2.drawPolygon(triangleX, triangleY, 3);
  }

  /**
   * Draws Sierpinski triangle
   * @param g2 The object to draw on
   * @param box The box to draw Sierpinski triangle
   */
  private void drawSierpinskiTriangle(Graphics2D g2, Rectangle2D box) {
    drawTriangle(g2, CANVAS_BOUNDS);
    drawInvertedTriangles(g2, CANVAS_BOUNDS, this.depth);
  }

  @Override
  protected void paintComponent(Graphics g) {
    super.paintComponent(g);
    Graphics2D g2 = (Graphics2D) g;
    drawSierpinskiTriangle(g2, CANVAS_BOUNDS);
  }

  /**
   * Returns the color of each triangle base on its depth
   * @param depth Current depth
   * @return Color of the triangle
   */
  private Color getColorByDepth(int depth) {
    Color color = colors[(this.depth - depth) % 3];
    return color;
  }

  public void setDepth(int depth) {
    this.depth = depth;
  }

  public void setColors(Color[] colors) {
    this.colors = colors;
  }
}
