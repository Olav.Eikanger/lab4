package no.uib.inf101.colorgrid;

import java.awt.Color;
import java.util.ArrayList;
import java.util.List;

public class ColorGrid implements IColorGrid {
	private int rows;
	private int columns;
	private CellColor[][] grid;

	public ColorGrid(int rows, int columns) {
		this.rows = rows;
		this.columns = columns;
		this.grid = new CellColor[rows][columns];
	}

	@Override
	public int rows() {
		return rows;
	}

	@Override
	public int cols() {
		return columns;
	}

	@Override
	public List<CellColor> getCells() {
		List<CellColor> list = new ArrayList<>();
		for (int i = 0; i < this.grid.length; i++) {
			CellColor[] row = this.grid[i];
			for (int j = 0; j < row.length; j++) {
				CellColor columnValue = row[j];
				if (columnValue == null) {
					columnValue = new CellColor(new CellPosition(i, j), null);
				}
				list.add(columnValue);
			}
		}

		return list;
	}

	@Override
	public Color get(CellPosition pos) {
		CellColor entry = this.grid[pos.row()][pos.col()];
		return entry != null ? entry.color() : null;
	}

	@Override
	public void set(CellPosition pos, Color color) {
		this.grid[pos.row()][pos.col()] = new CellColor(pos, color);
	}
}
