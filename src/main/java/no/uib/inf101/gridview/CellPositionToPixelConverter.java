package no.uib.inf101.gridview;

import java.awt.geom.Rectangle2D;

import no.uib.inf101.colorgrid.CellPosition;
import no.uib.inf101.colorgrid.GridDimension;

public class CellPositionToPixelConverter {
  private Rectangle2D box;
  private GridDimension gd;
  private double margin;

  public CellPositionToPixelConverter(Rectangle2D box, GridDimension gd, double margin) {
    this.box = box;
    this.gd = gd;
    this.margin = margin;
  }

  public Rectangle2D getBoundsForCell(CellPosition position){
    double cellWidth = (box.getWidth() - margin * (gd.cols() + 1)) / gd.cols() ;
    double cellHeight = (box.getHeight()  - margin * (gd.rows() + 1)) / gd.rows();
    double cellX = box.getX() + position.col() * cellWidth + (position.col() + 1) * margin;
    double cellY = box.getY() + position.row() * cellHeight + (position.row() + 1) * margin ;
    Rectangle2D rectangle = new Rectangle2D.Double(cellX, cellY, cellWidth, cellHeight);
    return rectangle;
  }
}
