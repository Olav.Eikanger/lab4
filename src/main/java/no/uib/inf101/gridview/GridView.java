package no.uib.inf101.gridview;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.geom.Rectangle2D;


import javax.swing.JPanel;

import no.uib.inf101.colorgrid.CellColor;
import no.uib.inf101.colorgrid.CellColorCollection;
import no.uib.inf101.colorgrid.IColorGrid;

public class GridView extends JPanel {
	private IColorGrid grid;
	private static final double OUTER_MARGIN = 30;
	private static final double CELL_MARGIN = 30;
	private static final Color MARGIN_COLOR = Color.LIGHT_GRAY;
	private static final Color NULL_COLOR = Color.DARK_GRAY;

	public GridView(IColorGrid grid) {
		this.grid = grid;
		this.setPreferredSize(new Dimension(400, 300));
	}

	@Override
	protected void paintComponent(Graphics g) {
		super.paintComponent(g);
		Graphics2D g2 = (Graphics2D) g;
		drawGrid(g2);
	}

	private void drawGrid(Graphics2D g2){
		double boxX = this.getX() + OUTER_MARGIN;
		double boxY = this.getY() + OUTER_MARGIN;
		double boxWidth = this.getWidth() - 2 * OUTER_MARGIN;
		double boxHeight = this.getHeight() - 2 * OUTER_MARGIN;
		Rectangle2D box = new Rectangle2D.Double(boxX, boxY, boxWidth, boxHeight);
		g2.setColor(MARGIN_COLOR);
		g2.fill(box);
		CellPositionToPixelConverter converter = 
		new CellPositionToPixelConverter(box, this.grid, CELL_MARGIN);
		drawCells(g2, grid, converter);
	}

	private static void drawCells(
		Graphics2D g2,
		CellColorCollection cells,
		CellPositionToPixelConverter converter
	){
		for (CellColor cell : cells.getCells()) {
			Rectangle2D bounds = converter.getBoundsForCell(cell.cellPosition());
			g2.setColor(cell.color() != null ? cell.color() : NULL_COLOR);
			g2.fill(bounds);
		}
	}
}
